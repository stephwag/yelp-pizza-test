module SearchHelper
  def get_review_data(business_id, limit = 10)
    page = MetaInspector.new(
      "https://www.yelp.com/biz/#{business_id}", 
      download_images: false, 
      headers: {'User-Agent' => UserAgents.rand()})
    
    review_data = page.parsed.css('div.review-content').take(limit)
    reviews = []
    rating_sum = 0

    review_data.each do |r|
      rating_text = r.css('div.i-stars').first.attributes['title'].value
      rating_sum += rating_text.gsub(' star rating', '').to_f

      reviews << {
        rating: rating_text,
        text: r.css('p').text
      }
    end

    rating_average = rating_sum / review_data.count

    { average: rating_average, reviews: reviews }
  end
end
