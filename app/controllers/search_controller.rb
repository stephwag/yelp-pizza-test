class SearchController < ApplicationController
  include SearchHelper
  respond_to :html, :json
  
  def index
  end

  def search
    params = { 
      term: search_params[:query],
            limit: 10,
          category_filter: 'pizza'
         }

    @results = Yelp.client.search('New York', params, { lang: 'en' })

    respond_to do |format|
      format.js { render layout: false }
    end
  end

  def refresh_reviews
    @business_id = params[:business_id]
    @limit = params[:search].present? ? search_params[:limit].to_i : 10

    result = get_review_data(params[:business_id], @limit)
    @rating_average = result[:average]
    @reviews = result[:reviews]

    respond_to do |format|
      format.js { render layout: false }
    end
  end

  def reviews
    @business_id = params[:business_id]
    result = get_review_data(@business_id)
    @rating_average = result[:average]
    @reviews = result[:reviews]

    respond_to do |format|
      format.html { respond_modal_with @reviews }
    end
  end

  private

  def search_params
    params.require(:search).permit(:query, :limit, :business_id)
  end
end
